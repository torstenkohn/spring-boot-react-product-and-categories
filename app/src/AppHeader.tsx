import React from "react";
import {makeStyles} from "@material-ui/core";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import Link from "@material-ui/core/Link";


const useStyles = makeStyles(theme => ({
    toolbar: {
        borderBottom: `1px solid ${theme.palette.divider}`,
    },
    toolbarTitle: {
        flex: 1,
    },
    toolbarSecondary: {
        justifyContent: 'flex-start',
        overflowX: 'auto',
    },
    toolbarLink: {
        padding: theme.spacing(1),
        flexShrink: 0,
    },
}));

const sections = [
    {
        name: 'Products',
        path: '/',
    },
    {
        name: 'Categories',
        path: '/categories',
    },
];


export default function AppHeader() {
    const classes = useStyles();
    return <>
        <Toolbar className={classes.toolbar}>
            <Typography component="h2" variant="h5" color="inherit" align="center" noWrap
                        className={classes.toolbarTitle}>
                Products and Categories
            </Typography>
        </Toolbar>
        <Toolbar component="nav" variant="dense" className={classes.toolbarSecondary}>
            {sections.map(section => (
                <Link
                    color="inherit"
                    noWrap
                    key={section.name}
                    variant="body2"
                    href={section.path}
                    className={classes.toolbarLink}>
                    {section.name}
                </Link>
            ))}
        </Toolbar>
    </>;
}