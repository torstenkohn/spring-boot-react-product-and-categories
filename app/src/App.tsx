import React, {Component} from 'react';
import ProductList from "./products/ProductList";
import Categories from "./categories/Categories";
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom';

class App extends Component {
    render() {
        return (
            <Router>
                <Switch>
                    <Route path='/' exact={true} component={ProductList}/>
                    <Route path='/products' exact={true} component={ProductList}/>
                    <Route path='/categories' exact={true} component={Categories}/>
                </Switch>
            </Router>
        )
    }
}

export default App;
