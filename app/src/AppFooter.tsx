import Container from "@material-ui/core/Container";
import Typography from "@material-ui/core/Typography";
import React from "react";
import {makeStyles} from "@material-ui/core";

const useStyles = makeStyles(theme => ({
    footer: {
        backgroundColor: theme.palette.background.paper,
        marginTop: theme.spacing(8),
        padding: theme.spacing(6, 0),
    },
}));


export default function AppFooter() {
    const classes = useStyles();
    return <>
        <footer className={classes.footer}>
            <Container maxWidth="lg">
                <Typography variant="subtitle1" align="center" color="textSecondary" component="p">
                    © Torsten Kohn 2019
                </Typography>
            </Container>
        </footer>
    </>;
}