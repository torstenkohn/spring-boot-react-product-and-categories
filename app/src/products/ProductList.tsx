import {makeStyles} from "@material-ui/core";
import CssBaseline from "@material-ui/core/CssBaseline";
import Container from "@material-ui/core/Container";
import AppHeader from "../AppHeader";
import Grid from "@material-ui/core/Grid";
import CategoriesSidebar from "../categories/CategoriesSidebar";
import AppFooter from "../AppFooter";
import React, {Component} from "react";
import {Product} from "./Product";
import Typography from "@material-ui/core/Typography";
import Paper from "@material-ui/core/Paper";
import Table from "@material-ui/core/Table";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import TableCell from "@material-ui/core/TableCell";
import TableBody from "@material-ui/core/TableBody";
import IconButton from "@material-ui/core/IconButton";
import {DeleteForever} from "@material-ui/icons";

function useStyles() {
  return makeStyles(theme => ({
    mainGrid: {
      marginTop: theme.spacing(3),
    },
  }))
}

interface Props {
}

interface State {
  products: Product[],
  isLoading: boolean,
  error: any
}


class ProductList extends Component<Props, State> {
  private classes: any;

  constructor(props: any) {
    super(props);

    this.state = {
      products: [],
      isLoading: false,
      error: null
    };
    this.classes = useStyles();
    this.remove = this.remove.bind(this);
  }


  componentDidMount(): void {
    this.setState({isLoading: true});
    fetch('api/products')
    .then(response => {
      if (response.ok) {
        return response.json() as Promise<Product[]>;
      }
      console.log('Can not load products: ' + response.statusText);
      throw new Error(response.statusText)
    })
    .then(data => this.setState({products: data, isLoading: false}))
    .catch(error => {
      this.setState({error: error, isLoading: false})
    });
  }

  async remove(id: number) {
    await fetch(`/api/products/${id}`, {
      method: 'DELETE',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      }
    }).then(() => {
      let updatedProducts = [...this.state.products].filter(i => i.id !== id);
      this.setState({products: updatedProducts});
    });
  }

  render() {

    const {products, isLoading, error} = this.state;
    if (error) {
      return <p>{error.message}</p>
    }

    if (isLoading) {
      return <p>Loading...</p>
    }
    return <>
      <CssBaseline/>
      <Container maxWidth="lg">
        <AppHeader/>
        <main>
          <Grid container spacing={5} className={this.classes.mainGrid}>
            <CategoriesSidebar/>
            <Grid item xs={12} md={10}>
              <Typography variant="h6" gutterBottom>
                Products
              </Typography>
              <Paper>
                <Table>
                  <TableHead>
                    <TableRow>
                      <TableCell>Product Name</TableCell>
                      <TableCell align="right">Price</TableCell>
                      <TableCell align="right">Action</TableCell>
                    </TableRow>
                  </TableHead>
                  <TableBody>
                    {products.map(product => (
                        <TableRow key={product.id}>
                          <TableCell component="th" scope="row">
                            {product.name}
                          </TableCell>
                          <TableCell align="right">{product.price}</TableCell>
                          <TableCell align="center">
                            <IconButton onClick={() => this.remove(product.id)}>
                              <DeleteForever color="primary" />
                            </IconButton>
                          </TableCell>
                        </TableRow>
                    ))}
                  </TableBody>
                </Table>
              </Paper>
            </Grid>
          </Grid>
        </main>
      </Container>
      <AppFooter/>
    </>;
  }
}

export default ProductList;