import Typography from "@material-ui/core/Typography";
import Link from "@material-ui/core/Link";
import Grid from "@material-ui/core/Grid";
import React from "react";
import {makeStyles} from "@material-ui/core";

const useStyles = makeStyles(theme => ({
  sidebarSection: {
    marginTop: theme.spacing(3),
  },
}));


const categories: string[] = [];
export default function CategoriesSidebar() {
  const classes = useStyles();
  return <>
    <Grid item xs={12} md={2}>
      <Typography variant="h6" gutterBottom className={classes.sidebarSection}>
        Categories
      </Typography>
      {categories.map(item => (
          <Link display="block" variant="body1" href="#" key={item}>
            {item}
          </Link>
      ))}
    </Grid>
  </>;
}