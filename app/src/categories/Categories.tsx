import {makeStyles} from "@material-ui/core";
import CssBaseline from "@material-ui/core/CssBaseline";
import Container from "@material-ui/core/Container";
import AppHeader from "../AppHeader";
import Grid from "@material-ui/core/Grid";
import CategoriesSidebar from "../categories/CategoriesSidebar";
import Typography from "@material-ui/core/Typography";
import AppFooter from "../AppFooter";
import React from "react";

const useStyles = makeStyles(theme => ({
    mainGrid: {
        marginTop: theme.spacing(3),
    },
}));


export default function Categories() {
    const classes = useStyles();

    return <>
        <CssBaseline/>
        <Container maxWidth="lg">
            <AppHeader/>
            <main>
                <Grid container spacing={5} className={classes.mainGrid}>
                    <CategoriesSidebar/>
                    {/* Main content */}
                    <Grid item xs={12} md={8}>
                        <Typography variant="h6" gutterBottom>
                            {/* Title of the section e.g. Products */}
                            Categories
                        </Typography>
                        {/* Content goes here */}
                        Table of Categories
                    </Grid>
                </Grid>
            </main>
        </Container>
        <AppFooter/>
    </>;
}