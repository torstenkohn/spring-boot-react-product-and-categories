DROP TABLE IF EXISTS product;

CREATE TABLE product
(
    id          BIGINT AUTO_INCREMENT NOT NULL PRIMARY KEY,
    name        VARCHAR(50)           NOT NULL,
    description VARCHAR(250)          NOT NULL,
    price       INT                   NOT NULL
);

INSERT INTO product
VALUES (1, 'Product 1', 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed', 100),
       (2, 'Product 2', 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed', 200),
       (3, 'Product 3', 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed', 300),
       (4, 'Product 4', 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed', 400),
       (5, 'Product 5', 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed', 500);