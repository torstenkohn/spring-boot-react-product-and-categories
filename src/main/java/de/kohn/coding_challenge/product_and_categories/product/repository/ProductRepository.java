package de.kohn.coding_challenge.product_and_categories.product.repository;

import static de.kohn.coding_challenge.product_and_categories.db.public_.tables.Product.PRODUCT;

import de.kohn.coding_challenge.product_and_categories.db.public_.tables.records.ProductRecord;
import de.kohn.coding_challenge.product_and_categories.product.domain.ProductDTO;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;
import lombok.extern.slf4j.Slf4j;
import org.jooq.DSLContext;
import org.springframework.stereotype.Repository;

@Repository
@Slf4j
public class ProductRepository implements ProductCrudRepository<ProductDTO, Long> {

  private final DSLContext dsl;

  public ProductRepository(DSLContext dsl) {
    this.dsl = dsl;
  }

  @Override
  public ProductDTO save(ProductDTO product) {
    log.info("save product {}", product);
    Objects.requireNonNull(product, "A null as product can not be stored");
    ProductRecord productRecord;
    if (Objects.isNull(product.getId())) {
      productRecord = dsl.newRecord(PRODUCT);
    } else {
      productRecord = dsl.fetchOne(PRODUCT, PRODUCT.ID.eq(product.getId()));
    }

    productRecord.setName(product.getName());
    productRecord.setDescription(product.getDescription());
    productRecord.setPrice(product.getPrice());
    productRecord.store();

    return findOne(productRecord.getId());
  }

  @Override
  public ProductDTO findOne(Long id) {
    log.info("find one for id {}", id);
    return Optional.ofNullable(
        dsl
            .select(PRODUCT.asterisk())
            .from(PRODUCT)
            .where(PRODUCT.ID.eq(id))
            .fetchOneInto(ProductRecord.class)).map(this::convert)
        .orElse(null);
  }

  @Override
  public Collection<ProductDTO> findAll() {
    return dsl
        .select(PRODUCT.asterisk())
        .from(PRODUCT)
        .fetchInto(ProductRecord.class)
        .stream()
        .map(this::convert)
        .collect(Collectors.toList());
  }

  @Override
  public void delete(ProductDTO product) {
    log.info("delete product {}", product);
    Long productId = Objects
        .requireNonNull(product, "A null as product can not be deleted")
        .getId();

    dsl
        .delete(PRODUCT)
        .where(PRODUCT.ID.eq(productId))
        .execute();
  }

  private ProductDTO convert(ProductRecord record) {
    return new ProductDTO(
        record.getId(),
        record.getName(),
        record.getDescription(),
        record.getPrice());
  }
}
