package de.kohn.coding_challenge.product_and_categories.product.web;

import de.kohn.coding_challenge.product_and_categories.product.domain.ProductDTO;
import de.kohn.coding_challenge.product_and_categories.product.repository.ProductRepository;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import java.util.Collection;
import java.util.Objects;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/products")
@Api(value = "Manage Products", description = "Management of products with different operations")
public class ProductController {

  private final ProductRepository repository;

  ProductController(ProductRepository repository) {
    this.repository = repository;
  }

  @ApiOperation(value = "View the detail product for a given ID")
  @ApiResponses(value = {
      @ApiResponse(code = 200, message = "Successfully retrieved product"),
      @ApiResponse(code = 404, message = "The product you were trying to reach is not found")
  })
  @GetMapping("/{productId}")
  ResponseEntity<ProductDTO> findOne(@PathVariable Long productId) {
    ProductDTO product = repository.findOne(productId);
    if (Objects.isNull(product)) {
      return ResponseEntity.notFound().build();
    }
    return ResponseEntity.ok(product);
  }

  @ApiOperation(value = "View a list of available products")
  @ApiResponses(value = {
      @ApiResponse(code = 200, message = "Successfully retrieved list of products")
  })
  @GetMapping
  ResponseEntity<Collection<ProductDTO>> all() {
    return ResponseEntity.ok().body(repository.findAll());
  }

  @ApiOperation(value = "Create or update an given product")
  @ApiResponses(value = {
      @ApiResponse(code = 200, message = "Successfully stored the given product"),
      @ApiResponse(code = 400, message = "The given product is not valid for the system")
  })
  @PostMapping
  ResponseEntity<ProductDTO> save(@RequestBody ProductDTO product) {
    ProductDTO result = repository.save(product);
    if (Objects.isNull(result)) {
      return ResponseEntity.badRequest().build();
    }
    return ResponseEntity.ok(result);
  }

  @ApiOperation(value = "Delete the product with a given ID")
  @ApiResponses(value = {
      @ApiResponse(code = 200, message = "Successfully deleted the product"),
      @ApiResponse(code = 404, message = "There exists no product with the given ID")
  })
  @DeleteMapping("/{productId}")
  ResponseEntity<?> delete(@PathVariable Long productId) {

    ResponseEntity<ProductDTO> response = findOne(productId);
    if (response.getStatusCode().isError()) {
      return response;
    }

    repository.delete(response.getBody());
    return ResponseEntity.ok().build();
  }
}
