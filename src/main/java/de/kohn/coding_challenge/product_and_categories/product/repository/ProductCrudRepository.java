package de.kohn.coding_challenge.product_and_categories.product.repository;

import java.io.Serializable;

public interface ProductCrudRepository<T, ID extends Serializable> {

    T save(T entity);

    T findOne(ID primaryKey);

    Iterable<T> findAll();

    void delete(T entity);
}
