package de.kohn.coding_challenge.product_and_categories.product.web;


import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.contains;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.content;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.fasterxml.jackson.databind.ObjectMapper;
import de.kohn.coding_challenge.product_and_categories.product.domain.ProductDTO;
import de.kohn.coding_challenge.product_and_categories.product.repository.ProductRepository;
import java.util.Arrays;
import java.util.List;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

@RunWith(SpringRunner.class)
@WebMvcTest
public class ProductControllerTest {

  @MockBean
  private ProductRepository repository;

  @Autowired
  private MockMvc mockMvc;


  @Test
  public void should_return_all_products() throws Exception {
    List<ProductDTO> expectedResult = Arrays
        .asList(
            new ProductDTO(1L, "product 1", "description 1", 100),
            new ProductDTO(2L, "product 2", "description 2", 100)
        );

    when(repository.findAll()).thenReturn(expectedResult);

    String jsonResult = mockMvc
        .perform(get("/api/products").accept(MediaType.APPLICATION_JSON_UTF8))
        .andExpect(status().isOk())
        .andReturn()
        .getResponse()
        .getContentAsString();

    assertThat(jsonResult).isEqualTo(new ObjectMapper().writeValueAsString(expectedResult));
  }

  @Test
  public void should_return_product_for_valid_id() throws Exception {
    final ProductDTO expectedResult = new ProductDTO(1L, "product 1", "description 1", 100);
    when(repository.findOne(1L)).thenReturn(expectedResult);

    String jsonResult = mockMvc
        .perform(get("/api/products/1").accept(MediaType.APPLICATION_JSON_UTF8))
        .andExpect(status().isOk())
        .andReturn()
        .getResponse()
        .getContentAsString();

    assertThat(jsonResult).isEqualTo(new ObjectMapper().writeValueAsString(expectedResult));
  }

  @Test
  public void should_return_not_found_for_invalid_id_in_find_one() throws Exception {
    when(repository.findOne(1L)).thenReturn(null);

    mockMvc
        .perform(get("/api/products/1").accept(MediaType.APPLICATION_JSON_UTF8))
        .andExpect(status().isNotFound());
  }

  @Test
  public void should_return_status_ok_for_valid_id_in_delete() throws Exception {
    final ProductDTO expectedResult = new ProductDTO(1L, "product 1", "description 1", 100);
    when(repository.findOne(1L)).thenReturn(expectedResult);

    mockMvc
        .perform(delete("/api/products/1").accept(MediaType.APPLICATION_JSON_UTF8))
        .andExpect(status().isOk());
  }

  @Test
  public void should_return_not_found_for_invalid_id_in_delete() throws Exception {
    when(repository.findOne(1L)).thenReturn(null);

    mockMvc
        .perform(delete("/api/products/1").accept(MediaType.APPLICATION_JSON_UTF8))
        .andExpect(status().isNotFound());
  }
}