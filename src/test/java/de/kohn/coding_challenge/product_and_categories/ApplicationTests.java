package de.kohn.coding_challenge.product_and_categories;

import static org.assertj.core.api.Assertions.assertThat;

import de.kohn.coding_challenge.product_and_categories.product.web.ProductController;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ApplicationTests {

	@Autowired
	private ProductController productController;

	@Test
	public void contextLoads() {
		assertThat(productController).isNotNull();
	}

}
