# spring-boot-react-product-and-categories

Fullstack project with Spring Boot in the backend and React in the frontend.

## Quick access when the application is running

* Swagger UI: http://localhost:8080/swagger-ui.html
* API Endpoint for products: http://localhost:8080/api/products
* Frontend
  * Frontend as own app: http://localhost:3000
  * Frontend as part of the backend: http://localhost:8080

## Prerequisites

* Java Version 11 (e.g. Open JDK) is installed
  ```bash
  # to check the Java version
  java -version
  ```
* Maven should use Java Version 11
  ```bash
  # To check the version of maven use following command:
  mvn -v
  
  # The output could look like this:
  Maven home: /home/torsten/.sdkman/candidates/maven/current
  Java version: 11.0.3, vendor: Eclipse OpenJ9, runtime: /home/torsten/.sdkman/candidates/java/11.0.3.j9-adpt
  Default locale: en_GB, platform encoding: UTF-8
  OS name: "linux", version: "5.0.0-21-generic", arch: "amd64", family: "unix"
  
  # The maven wrapper can also be used as an alternative with:
  ./mvnw -v
  ```
* (Optional) Yarn and NodeJS has to be installed to run the frontend without the maven plugin
  This is only needed if you want to start the frontend directly with yarn.
  
## Build and run the application with the frontend locally

### Start the backend with IntelliJ or maven
This section will describe how to run the backend as own applications.

**Option #1**: Start the Application with the default configuration with IntelliJ.
This is only working, when the jOOQ classes are generated.
To generate the classes use the command: `mvn package` or `./mvnw package`

**Option #2** Start the spring-boot application with maven
```bash
# use locally installed maven
mvn spring-boot:run

# use maven wrapper
./mvnw spring-boot:run
```

### Start the frontend with yarn
```bash
cd app && yarn start
```

### Run frontend and backend as one application

The react app will be delivered by the spring-boot application.
You can start the app with the following command:
```bash
# use locally installed maven
mvn spring-boot:run -Pprod

# use maven wrapper
./mvnw spring-boot:run -Pprod
```